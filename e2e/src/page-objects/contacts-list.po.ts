import { by, element, ElementArrayFinder, ElementFinder, promise } from 'protractor';

export class ContactsListPageObject {
  public container: ElementFinder;
  public list: ElementArrayFinder;

  constructor() {
    this.container = element(by.tagName('contacts-list'));
    this.list      = this.container.all(by.tagName('li'));
  }

  getContact(index: number): ElementFinder {
    return this.list.get(index);
  }

  selectContact(index): promise.Promise<void> {
    const contact      = this.getContact(index);
    const contactLinks = contact.all(by.tagName('a'));
    return contactLinks.get(0).click();
  }

  getContactClass(index): promise.Promise<string> {
    const contact = this.getContact(index);
    return contact.getAttribute('class');
  }
}
