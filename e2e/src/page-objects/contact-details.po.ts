import { by, element, ElementFinder } from 'protractor';

export class ContactDetailsPageObject {
  public container: ElementFinder;
  public form: ElementFinder;
  public formCancelButton: ElementFinder;
  public formEditButton: ElementFinder;

  constructor() {
    this.container        = element(by.tagName('contact-details'));
    this.form             = this.container.element(by.tagName('form'));
    this.formCancelButton = this.form.element(by.id('contact-details__cancel'));
    this.formEditButton   = this.container.element(by.id('contact-details__edit'));
  }

  public getField(fieldName: string): ElementFinder {
    return this.form.element(by.name(fieldName));
  }


  public fillOutField(fieldName: string, text: string) {
    this.form.element(by.name(fieldName)).sendKeys(text);
  }

  public clearOutField(fieldName: string) {
    this.form.element(by.name(fieldName)).clear();
  }
}
