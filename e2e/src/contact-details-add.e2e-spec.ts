import { browser, by, element } from 'protractor';

const helpers = require('protractor-helpers');

describe('Editing a contact', () => {
  beforeAll(() => {
    browser.get('#');
  });

  it('Should open a form for adding a contact', () => {
    const contactsComponent = element(by.tagName('contacts'));
    const addButton         = contactsComponent.element(by.id('add'));
    addButton.click();

    const detailsContainer = element(by.tagName('contact-details'));
    const detailsForm      = detailsContainer.element(by.tagName('form'));
    const contactAddButton = detailsForm.element(by.css('input[type=submit]'));
    expect(detailsForm.isDisplayed()).toBeTruthy();
    expect(contactAddButton.isEnabled()).toBeFalsy();

  });

  it('Should cancel the form', () => {
    const detailsContainer = element(by.tagName('contact-details'));
    const detailsForm      = detailsContainer.element(by.tagName('form'));
    const cancelButton     = detailsForm.element(by.id('contact-details__cancel'));
    browser.executeScript('arguments[0].scrollIntoView();', cancelButton.getWebElement());
    cancelButton.click();

    expect(detailsForm.isPresent()).toBeFalsy();
  });

  it('Should fill out first name', () => {
    //
    const contactsComponent = element(by.tagName('contacts'));
    const addButton         = contactsComponent.element(by.id('add'));
    browser.executeScript('arguments[0].scrollIntoView();', addButton.getWebElement());
    addButton.click();
    //
    const detailsContainer = element(by.tagName('contact-details'));
    const detailsForm      = element(by.tagName('form'));
    const field            = detailsForm.element(by.name('firstName'));

    field.sendKeys('Hello');

    expect(field.getAttribute('class')).toContain('ng-valid');
  });

  it('Should fill out last name', () => {
    const detailsContainer = element(by.tagName('contact-details'));
    const detailsForm      = element(by.tagName('form'));
    const field            = detailsForm.element(by.name('lastName'));
    field.sendKeys('World');

    expect(helpers.hasClass(field, 'ng-valid')).toBeTruthy();
  });

  it('Should validate the email properly', () => {
    const detailsContainer = element(by.tagName('contact-details'));
    const detailsForm      = element(by.tagName('form'));
    const field            = detailsForm.element(by.name('email'));

    field.sendKeys('Oops');

    expect(field.getAttribute('class')).toContain('ng-invalid');
  });

  it('Should fill out the email', () => {
    const detailsContainer = element(by.tagName('contact-details'));
    const detailsForm      = element(by.tagName('form'));
    const field            = detailsForm.element(by.name('email'));

    field.clear();
    field.sendKeys('properemail@google.com');

    expect(field.getAttribute('class')).toContain('ng-valid');
  });

  it('Should have an enabled submit button', () => {
    const detailsContainer = element(by.tagName('contact-details'));
    const detailsForm      = detailsContainer.element(by.tagName('form'));
    const contactAddButton = detailsForm.element(by.css('input[type=submit]'));

    expect(contactAddButton.isEnabled()).toBeTruthy();
  });
});
