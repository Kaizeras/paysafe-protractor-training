import { browser } from 'protractor';

describe('Page title', () => {

  beforeAll(() => {
    browser.get('#');
  });

  it('Should have the correct title', () => {
    expect(browser.getTitle()).toContain('Angular Training App');
  });
});
