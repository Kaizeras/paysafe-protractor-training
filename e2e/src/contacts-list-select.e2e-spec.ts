import { browser } from 'protractor';
import { ContactsListPageObject } from './page-objects/contacts-list.po';
import { ContactDetailsPageObject } from './page-objects/contact-details.po';

describe('Selecting a contact from the list', () => {
  let contactsListPo: ContactsListPageObject;
  let contactsDetailsPo: ContactDetailsPageObject;

  beforeAll(() => {
    browser.get('#');
    contactsListPo    = new ContactsListPageObject();
    contactsDetailsPo = new ContactDetailsPageObject();
  });

  it('Should have a contacts list present', () => {
    expect(contactsListPo.list.count()).toEqual(5);
  });

  it('Should select a contact from the list', () => {
    contactsListPo.selectContact(1);
    expect(contactsListPo.getContactClass(1)).toContain('active');
  });

  it('Should have the details container present', () => {
    expect(contactsDetailsPo.container.isPresent()).toBeTruthy();
  });

  it('Should have the correct contact details present in the details container', () => {
    expect(contactsDetailsPo.container.getText()).toContain('Alyx');
  });

  it('Should have the edit button present', () => {
    expect(contactsDetailsPo.formEditButton.isPresent()).toBeTruthy();
  });

});
